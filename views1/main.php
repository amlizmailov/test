<!DOCTYPE html>
<html>

	<head>
		<title><?php echo $title ?: "" ?></title>
		<?php
			if(!empty($css)) {
				if(is_array($css)) {
					foreach ($css as $key => $item) {
						echo sprintf("<link rel='stylesheet' href='%s'>%s", $item, PHP_EOL);
					}
				}
				if(is_string($css)) {
					echo sprintf("<link rel='stylesheet' href='%s'>", $css);
				}
			}
		?>
	</head>

	<body>

		<div class="container">

			<header class="header">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link" href="/">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/?page=contacts">Contacts</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/?page=about">About</a>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			<div class="main">
				<?php echo $render ?: "" ?>
			</div>

			<footer class="footer">
				<?php echo $footer ?: "" ?>
			</footer>

		</div>

		<!-- The Modal -->
		<div class="modal" id="feedback">
			<div class="modal-dialog">
				<div class="modal-content">

                    <form action="/">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Feedback</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->

                        <div class="modal-body">

                            <div class="form" id="form">

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Name" required="">
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="E-mail" required="">
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="description" placeholder="Description"></textarea>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Send</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>

                            </div>

                            <div id="form-success">
                                <div class="form-group">
                                    <label>Your message successfully sended</label>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>

                            <div id="form-fail">
                                <div class="form-group">
                                    <label>Something wrong :(</label>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>

                        </div>

                    </form>

                    <div class="">

				</div>
			</div>
		</div>

		<?php
			if(!empty($js)) {
				if(is_array($js)) {
					foreach ($js as $key => $item) {
						echo sprintf("<script src='%s'></script>%s", $item, PHP_EOL);
					}
				}
				if(is_string($js)) {
					echo sprintf("<script src='%s'></script>", $js);
				}
			}
		?>

	</body>
</html>