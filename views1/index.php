<div class="card">
	<div class="card-body">
		<?php echo $content ?: "" ?>

		<button type="button" class="btn btn-primary" id="btnFeedback">Feedback</button>
	</div>
</div>