<?php

error_reporting(E_ALL);

require_once __DIR__ . "/autoload.php";

$db = new DB($db_config);

if (count($_POST) > 0) {
    $feedback = new Feedback($db);
    $json = [
        'status' => 0
    ];

    $_POST['ip'] = $_SERVER['REMOTE_ADDR'];

    if ($feedback->save($_POST)) {
        $json = [
            'status' => 1
        ];
    }
    die(json_encode($json));
}

$template = new Template("views1");

$template->cache = false;
$template->cache_time = 2;
$template->renderCss([
    "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
    "/css/style.css",
]);
$template->renderJs([
    "https://code.jquery.com/jquery-3.3.1.min.js",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",
    "/js/common.js",
]);
$template->render($page, [
    "title" => "My App",
    "page" => $page,
    "content" => "",
    "footer" => null
]);


