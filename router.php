<?php

$availablePages = [];
$d = scandir("views1");
foreach ($d as $key => $value) {
	if(!in_array($value, [".", "..", "main.php", "404.php"])) {
		$availablePages[] = str_replace(".php", "", $value);
	}
}

$page = !empty($_GET["page"]) ? $_GET["page"] : "index";
if(!in_array($page, $availablePages)) {
	header("HTTP/1.0 404 Not Found");
	$page = "404";
}