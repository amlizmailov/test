CREATE TABLE feedback(
    `id` int(11) PRIMARY KEY AUTO_INCREMENT,
    `name` varchar(50),
    `description` varchar(50),
    `mark` int(1),
    `email` varchar(50),
    `ip` varchar(32) 
)