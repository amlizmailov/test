$(document).ready(function () {

    var STATUS_OK = 1;
    var STATUS_FAIL = 0;

    $(document).on("click", "#btnFeedback", function (e) {
        $("#feedback").show('modal');
    });

    $(document).on("click", "#feedback .btn-danger, #feedback .close", function (e) {
        $("#feedback").hide('modal');
    });

    $(document).on("click", "#feedback .btn-primary", function (e) {
        e.preventDefault();

        resetInputStyle();

        var $form = $("#feedback");

        var validate = true;
        $('#feedback form input, #feedback form textarea').each(function () {
            var email = false;
            if ($(this).attr('name') == 'email') {
                email = true;
            }
            if (!validateRequiredInput($(this).attr('name'), email) && $(this).attr('required') == 'required') {
                validate = false;
                return;
            }
        });

        if (validate) {
            $.ajax({
                url: $form.attr('action'),
                method: 'post',
                dataType: 'json',
                data: $form.find('form').serialize(),
                success: function (rsp) {
                    if (rsp.status == STATUS_OK) {
                        $form.find("#form").hide();
                        $form.find("#form-success").show();
                    }
                    if (rsp.status == STATUS_FAIL) {
                        $form.find("#form").hide();
                        $form.find("#form-fail").show();
                    }
                }
            });
            return true;
        }
        return false;
    });

});

function resetInputStyle() {
    $('#feedback form input, #feedback form textarea').each(function () {
        $(this).removeClass('form-control-error');
    });
}

function validateRequiredInput(name, rule_email) {
    var field = $('input[name="' + name + '"]');
    var value = field.val();

    if ($.trim(value) == '') {
        field.addClass('form-control-error');
        return false;
    }

    if (rule_email === true) {
        if (!validateEmail(value)) {
            field.addClass('form-control-error');
            return false;
        }
    }

    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}