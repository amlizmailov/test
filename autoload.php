<?php

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/router.php";

spl_autoload_register(function(string $class){
	$class_filename = __DIR__ . "/classes/" . $class . ".php";
	if(file_exists($class_filename)) {
		require_once $class_filename;
		return true;
	} 

	$class_filename = __DIR__ . "/classes/drivers/" . $class . ".php";
	if(file_exists($class_filename)) {
		require_once $class_filename;
		return true;
	}
});