<?php

/**
 * @property string $main_page
 * @property string $template_dir
 * @property string $cache_dir
 * @property bool $cache
 * @property mixed $css
 * @property mixed $js
 * @property int $cache_time
 */
class Template
{
    public $main_page = "main.php";
    public $template_dir = __DIR__ . "/../views/";
    public $cache_dir = "";
    public $cache = false;
    public $css = null;
    public $js = null;
    public $cache_time = 1;

    /**
     * @param string $template_dir
     **/
    public function __construct(string $template_dir = null)
    {
        if ($template_dir != null) {
            $this->template_dir = $template_dir;
        }
        $this->cache_dir = $_SERVER["DOCUMENT_ROOT"] . "/cache/";
    }

    /**
     * @param string $template_name
     * @param array $data
     **/
    public function render(string $template_name = "", array $data = [])
    {
        $main_filename = $this->template_dir . "/" . $this->main_page;

        if (!$this->checkTemplateExists($template_name)) {
            return;
        }

        // if cache exists - not render
        if ($this->getPageFromCache($template_name)) {
            return;
        }

        // render template
        $render = $this->renderPartial($template_name, $data);
        if (null == $render) {
            return;
        }

        // if cache enabled
        if ($this->cache) {
            ob_start();
        }

        // mapping variables
        extract($data);
        if ($this->css) {
            $css = $this->css;
        }
        if ($this->js) {
            $js = $this->js;
        }
        require_once $main_filename;

        // save cache to file
        if ($this->cache) {
            $cache_page = ob_get_contents();
            ob_end_clean();
            $this->saveCacheToFile($template_name, $cache_page);
            echo $cache_page;
        }
    }

    /**
     * @param  string $tmpl_filename
     * @param  array $data
     * @return string
     **/
    public function renderPartial(string $template_name = "", array $data = [])
    {
        $tmpl_filename = $this->template_dir . "/" . $template_name . ".php";
        $content = null;
        if (file_exists($tmpl_filename)) {
            // render template
            ob_start();
            extract($data);
            require_once $tmpl_filename;
            $content = ob_get_contents();
            ob_end_clean();
        }
        return $content;
    }

    /**
     * @param  string $template_name
     * @return bool
     **/
    private function checkTemplateExists(string $template_name)
    {
        $tmpl_filename = $this->template_dir . "/" . $template_name . ".php";
        return file_exists($tmpl_filename);
    }

    /**
     * @param string $page
     **/
    private function saveCacheToFile(string $template_name, string $page = null)
    {
        $f = fopen($this->cache_dir . $template_name . "_" . time(true) . ".html", "w");
        if ($f) {
            fwrite($f, $page);
            fclose($f);
        }
    }

    /**
     * @param  string $cache_filename
     * @return bool
     **/
    private function cacheExpired(string $cache_filename)
    {
        preg_match_all('/[0-9]{1,}/', $cache_filename, $matches);
        if (!empty($matches[0][0])) {
            list($time) = explode(".", $matches[0][0]);
            return time(true) - $time > $this->cache_time * 60;
        }
        return true;
    }

    /**
     * @param string $cache_filename
     **/
    private function cacheDelete(string $cache_filename)
    {
        unlink($this->cache_dir . "/" . $cache_filename);
    }

    /**
     * @param  string $template_name
     * @return bool
     **/
    private function getPageFromCache(string $template_name = null)
    {
        if ($template_name == null) return false;

        // look for cache
        $d = scandir($this->cache_dir);

        // return from cache
        if (count($d) > 2 && $this->cache) { // [".", ".."]
            $cacheFileNameIndex = null;
            foreach ($d as $index => $item) {
                list($t,) = explode("_", $item);
                if ($t == $template_name) {
                    $cacheFileNameIndex = $index;
                    break;
                }
            }
            if ($cacheFileNameIndex == null) return false;

            $cacheFileName = $d[$cacheFileNameIndex];

            if (!file_exists($this->cache_dir . $cacheFileName)) {
                return false;
            }

            // if cache expired - clear
            if ($this->cacheExpired($cacheFileName)) {
                $this->cacheDelete($cacheFileName);
                return false;
            }

            echo file_get_contents($this->cache_dir . $cacheFileName);
            return true;
        }
        return false;
    }

    /**
     * @param mixed $css
     **/
    public function renderCss(/*mixed*/
        $css = null)
    {
        $this->css = $css;
    }

    /**
     * @param mixed $js
     **/
    public function renderJs(/*mixed*/
        $js = null)
    {
        $this->js = $js;
    }

    /**
     * @param string $dir
     **/
    public function setCacheDir(string $dir)
    {
        $this->cache_dir = $dir;
    }

    /**
     * @return string $dir
     **/
    public static function getCacheDir()
    {
        return $this->cache_dir;
    }

    /**
     * @param string $template_dir
     **/
    public function setTemplateDir(string $template_dir)
    {
        $this->template_dir = $template_dir;
    }

    /**
     * @return string
     **/
    public static function getTemplateDir()
    {
        return $this->template_dir;
    }

}