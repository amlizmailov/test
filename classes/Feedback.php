<?php

/**
 * @property string $name
 * @property string $description
 * @property int $mark
 * @property string $email
 * @property string $ip
 * @property DB $db
 * @property string $table
 */
class Feedback
{
    public $name = null;
    public $description = null;
    public $mark = null;
    public $email = null;
    public $ip = null;
    private $db = null;
    private static $table = "feedback";

    /**
     * @param DB $db
     */
    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    private function rules()
    {
        return [
            'name' => 'is_string',
            'description' => 'is_string',
            'email' => 'is_string',
            'mark' => 'is_numeric',
            'ip' => 'is_string',
        ];
    }

    /**
     * @param  array $params
     * @return boolean
     **/
    public function save(array $params)
    {
        if (self::validate($params)) {
            return $this->db->insert(self::$table, array_keys($params), $params);
        }
        throw new Exception("Validation error", 1);
    }

    /**
     * @param  array $conditions
     * @return boolean
     **/
    public function delete(array $conditions)
    {
        return $this->db->delete(self::$table, $conditions);
    }

    /**
     * @param  array $params
     * @return boolean
     **/
    private function validate(array $params)
    {
        $rules = $this->rules();
        foreach ($params as $key => $value) {
            if (!property_exists(__CLASS__, $key) || !call_user_func($rules[$key], $value)) {
                throw new Exception("Error validation on field " . $key, 1);
                return false;
            }
        }
        return true;
    }

}