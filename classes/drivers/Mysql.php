<?php

/**
 * @property string  $sql
 */

class Mysql{

	private static $sql = null;

	/**
	 * @param  object $connection
	 * @param  array  $table
	 * @param  array  $attributes
	 * @param  array  $data
	 * @return mixed
	**/ 
	public static function insert(/*mixed*/ $connection, string $table, array $attributes, array $data)
	{
		foreach($data as &$item) {
			$item = "'" . $item . "'";
		}

		$sql  = sprintf(" INSERT INTO %s(%s)", $table, implode(",", $attributes));
		$sql .= sprintf(" VALUES(%s)", implode(",", $data));

		self::$sql = $sql;

		return mysqli_query($connection, $sql);
	}

	/**
	 * @param  object $connection
	 * @param  array  $table
	 * @param  array  $conditions
	 * @param  array  $params
	 * @return mixed
	**/ 
	public function update(/*mixed*/ $connection, string $table, array $conditions, array $params) 
	{
		$where = [];
		foreach ($conditions as $k => $v) {
			$where[] = "$k='$v'";
		}

		$data = [];
		foreach($params as $k => $v) {
			$data[] = "$k='$v'";
		}

		$sql  = sprintf(" UPDATE %s SET %s WHERE 1 = 1 AND %s ", $table, implode(" AND ", $where), implode(",", $data));
		
		self::$sql = $sql;
		
		return mysqli_query($connection, $sql);
	}

	/**
	 * @param  array $table
	 * @param  array $conditions
	 * @return mixed
	**/ 
	public static function delete(/*mixed*/ $connection, string $table, array $conditions)
	{
		if(count($conditions) == 0) return;
		
		$data = [];
		foreach($conditions as $k => $v) {
			$data[] = "$k='$v'";
		}

		$sql  = sprintf(" DELETE FROM %s WHERE 1 = 1 AND %s", $table, implode(" AND ", $data));
		
		self::$sql = $sql;
		
		return mysqli_query($connection, $sql);
	}

	/**
 	* @return string
	**/
	public static function getLastSql()
	{
		return static::$sql;
	}

}

	