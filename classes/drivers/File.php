<?php

class File
{
    /**
     * @param  object $connection
     * @param  array $table
     * @param  array $attributes
     * @param  array $data
     * @return null
     **/
    public static function insert(/*mixed*/
        $connection, string $table, array $attributes, array $data)
    {
        $file = $connection . $table . ".json";
        if (file_exists($file)) {
            $old_json = file_get_contents($file);
            $old_data = json_decode($old_json, true);
        } else {
            file_put_contents($file, "");
            $old_data = [];
        }

        $new_data = array_merge($old_data, [$data]);
        return file_put_contents($file, json_encode($new_data));
    }

    /**
     * @param  object $connection
     * @param  array $table
     * @param  array $conditions
     * @param  array $params
     * @return null
     **/
    public function update(/*mixed*/
        $connection, string $table, array $conditions, array $params)
    {
        return null;
    }

    /**
     * @param  array $table
     * @param  array $conditions
     * @return null
     **/
    public static function delete(/*mixed*/
        $connection, string $table, array $conditions)
    {
        $file = $connection . $table . ".json";
        if (file_exists($file)) {
            $old_json = file_get_contents($file);
            $old_data = json_decode($old_json, true);
        } else {
            return true;
        }

        if (!is_array($old_data)) {
            return false;
        }

        $new_data = [];

        foreach ($old_data as $k => $v) {
            $i = 0;
            foreach ($v as $name => $value) {
                foreach ($conditions as $k1 => $v1) {
                    if ($name == $k1 && $value == $v1) {
                        $i++;
                    }
                }
            }
            if ($i != count($conditions)) {
                $new_data[] = $v;
            }
        }

        file_put_contents($file, json_encode($new_data));
    }

    /**
     * @return string
     **/
    public static function getLastSql()
    {
        return null;
    }

}

	