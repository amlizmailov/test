<?php

/**
 * @property string $driver
 * @property object $connection
 * @property string $host
 * @property string $user
 * @property string $db
 * @property string $pass
 * @property string $port
 * @property string $db_dir
 */
class DB
{
    private $driver = "mysql";
    private $connection = null;

    private $host = null;
    private $user = null;
    private $db = null;
    private $pass = null;
    private $port = 3306;
    private $db_dir = null;

    public function __construct(array $db_config)
    {
        if (isset($db_config["driver"])) {
            $this->driver = $db_config["driver"];
        }

        foreach ($db_config as $key => $value) {
            $this->$key = $value;
        }

        switch ($this->driver) {
            default:
            case 'mysql':
                $this->connection = mysqli_connect($this->host, $this->user, $this->pass, $this->db, $this->port);
                break;
            case 'file':
                $this->connection = $this->db_dir;
                break;
        }

        $className = ucfirst($this->driver);
        return (new $className);
    }

    /**
     * @param  array $table
     * @param  array $attributes
     * @param  array $data
     * @return mixed
     **/
    public function insert(string $table, array $attributes, array $data)
    {
        return $this->driver::insert($this->connection, $table, $attributes, $data);
    }

    /**
     * @param  array $table
     * @param  array $conditions
     * @param  array $params
     * @return mixed
     **/
    public function update(string $table, array $conditions, array $params)
    {
        return $this->driver::update($this->connection, $table, $conditions, $params);
    }

    /**
     * @param  array $table
     * @param  array $conditions
     * @return mixed
     **/
    public function delete(string $table, array $conditions)
    {
        return $this->driver::delete($this->connection, $table, $conditions);
    }

    /**
     * @return string
     **/
    public function getLastSql()
    {
        return $this->driver::getLastSql();
    }

}